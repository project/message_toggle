Message Toggle
==============

The Message Toggle module adds SHOW/HIDE links to all Drupal messages. Clicking
these links within a message will toggle the visibility of that message.

Configuration
-------------
There is none. The links will appear once the module is enabled.

JavaScript
----------
All functionality is powered by jQuery so this module is completely dependent
on a given user having JavaScript enabled. Should JavaScript be disabled, the
markup for messages will not be affected in any way (graceful degradation).
